package ru.volkova.tm.api.entity;

import ru.volkova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
