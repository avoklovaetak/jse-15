package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.constant.ArgumentConst;
import ru.volkova.tm.constant.TerminalConst;
import ru.volkova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "show developer info"
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "show terminal commands"
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "show application version"
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "show system info"
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "close application"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "show program arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "show program commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "show task list"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "show project list"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "create new projects"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "clear all projects"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, "view project by id"
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, "view task by id"
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, "view project by name"
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, "view task by name"
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, "view project by index"
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, "view task by index"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "remove project by id"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "remove task by id"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "remove project by name"
    );

    private static final Command PROJECT_REMOVE_BY_ID_CASCADE = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID_CASCADE, null, "remove project and all project tasks by id"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "remove task by name"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "remove project by index"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "remove task by index"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "update project by index"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "update task by index"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "update project by id"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "update task by id"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null, "start project by id"
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null, "start task by id"
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null, "start project by name"
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null, "start task by name"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null, "start project by index"
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null, "start task by index"
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.PROJECT_FINISH_BY_ID, null, "finish project by id"
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.TASK_FINISH_BY_ID, null, "finish task by id"
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.PROJECT_FINISH_BY_NAME, null, "finish project by name"
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.TASK_FINISH_BY_NAME, null, "finish task by name"
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.PROJECT_FINISH_BY_INDEX, null, "finish project by index"
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.TASK_FINISH_BY_INDEX, null, "finish task by index"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, null, "change project status by id"
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID, null, "change task status by id"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME, null, "change project status by name"
    );

    private static final Command TASK_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_NAME, null, "change task status by name"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, null, "change project status by index"
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, null, "change task status by index"
    );

    private static final Command TASK_FIND_ALL_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_FIND_ALL_BY_PROJECT_ID, null, "find all project tasks by project id"
    );

    private static final Command TASK_BIND_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_BIND_BY_PROJECT_ID, null, "bind task to project by project id"
    );

    private static final Command TASK_UNBIND_BY_PROJECT_ID = new Command(
            TerminalConst.TASK_UNBIND_BY_PROJECT_ID, null, "unbind task from project by project id"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_VIEW_BY_ID, TASK_VIEW_BY_NAME, TASK_VIEW_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            TASK_START_BY_ID, TASK_START_BY_NAME, TASK_START_BY_INDEX,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_NAME, TASK_FINISH_BY_INDEX,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_NAME, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_FIND_ALL_BY_PROJECT_ID, TASK_BIND_BY_PROJECT_ID, TASK_UNBIND_BY_PROJECT_ID,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            PROJECT_START_BY_ID, PROJECT_START_BY_NAME, PROJECT_START_BY_INDEX,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_NAME, PROJECT_FINISH_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_NAME, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_REMOVE_BY_ID_CASCADE,
            EXIT
    };

    public Command[] getTerminalCommands() {
         return TERMINAL_COMMANDS;
    }

}
