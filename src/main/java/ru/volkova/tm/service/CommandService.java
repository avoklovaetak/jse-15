package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.api.service.ICommandService;
import ru.volkova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
