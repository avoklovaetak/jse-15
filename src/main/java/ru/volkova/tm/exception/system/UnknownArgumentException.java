package ru.volkova.tm.exception.system;

import ru.volkova.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Unknown argument...");
    }

}
