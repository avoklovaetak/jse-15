package ru.volkova.tm.enumerated;

public enum Status {

    NOT_STARTED("not started"),
    IN_PROGRESS("in progress"),
    COMPLETE("complete");

    Status(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;

    public String getDisplayName() {
        return displayName;
    }
}
